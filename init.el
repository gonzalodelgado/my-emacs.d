(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (url (concat (if no-ssl "http" "https") "://melpa.org/packages/")))
  (add-to-list 'package-archives (cons "melpa" url) t))
(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(global-wakatime-mode)

(setq inhibit-startup-screen t)
(setq package-selected-packages
      (quote
       (nov geiser racket-mode slime exec-path-from-shell paredit buffer-move wakatime-mode)))

;; Looksies
(when (window-system)
  (add-to-list 'default-frame-alist
	       '(font . "Mononoki-12"))
  (load-theme 'sanityinc-solarized-dark t))

;; MacOS specific config
(when (memq window-system '(mac ns))
  (setq mac-command-modifier 'control)
  (setq ns-right-alternate-modifier nil) ; make AltGR work as expected
  (exec-path-from-shell-initialize))

;; Use guile as scheme by default
(setq scheme-program-name "guile")
;; Use ipython as Python interpreter when available
(when (executable-find "ipython")
  (setq python-shell-interpreter "ipython"))
;; Use CLISP if available
(when (executable-find "clisp")
  (setq inferior-lisp-program "clisp"))
;; But if SBCL is there, use it instead
(when (executable-find "sbcl")
  (setq inferior-lisp-program "sbcl"))

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file)

(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)

;; nov-mode for reading epub ebooks
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
(defun my-nov-font-setup ()
  (face-remap-add-relative 'variable-pitch :family "Georgia"
			   :height 1.1))
(setq nov-text-width 72)
(add-hook 'org-mode-hook #'visual-line-mode)
(add-hook 'org-mode-hook #'org-display-inline-images)
(add-hook 'nov-mode-hook 'my-nov-font-setup)
(add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
(add-hook 'scheme-mode-hook           #'enable-paredit-mode)
(add-hook 'racket-mode-hook           #'enable-paredit-mode)

;; Custom keys
;; TODO: consider using Super or Hyper with these
(global-set-key (kbd "C-x p") 'previous-multiframe-window)

;; Custom commands
